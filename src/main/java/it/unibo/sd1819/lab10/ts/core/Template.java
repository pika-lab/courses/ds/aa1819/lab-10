package it.unibo.sd1819.lab10.ts.core;

import java.util.Optional;

public interface Template {

    default boolean matches(Tuple tuple) {
        return matchWith(tuple).isSuccess();
    }

    Match matchWith(Tuple tuple);

    interface Match {

        boolean isSuccess();

        <X> Optional<X> get(Object key);

    }
}
