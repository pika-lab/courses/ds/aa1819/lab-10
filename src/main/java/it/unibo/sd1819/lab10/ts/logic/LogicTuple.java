package it.unibo.sd1819.lab10.ts.logic;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.sd1819.lab10.ts.core.Tuple;

import java.util.Objects;

public class LogicTuple implements Tuple, Comparable<LogicTuple> {
    static final String TUPLE_WRAPPER = "tuple";
    private final Struct term;

    public LogicTuple(final String term) {
        this(Term.createTerm(Objects.requireNonNull(term)));
    }

    public LogicTuple(final Term term) {
        Objects.requireNonNull(term);
        if (term instanceof Struct && ((Struct) term).getName().equals(TUPLE_WRAPPER) && ((Struct) term).getArity() == 1) {
            this.term = (Struct) term;
        } else {
            this.term = new Struct(TUPLE_WRAPPER, term);
        }
    }

    @Override
    public String toString() {
        return term.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((term == null) ? 0 : term.toString().hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final LogicTuple other = (LogicTuple) obj;
        if (term == null) {
            return other.term == null;
        } else return term.toString().equals(other.term.toString());
    }

    public Struct getTerm() {
        return term;
    }

    public Term getTuple() {
        return getTerm().getArg(0);
    }

    @Override
    public int compareTo(LogicTuple o) {
        return getTuple().toString().compareTo(o.getTuple().toString());
    }
}
