package it.unibo.sd1819.lab10.ts.logic;

import it.unibo.sd1819.lab10.ts.core.ExtendedTupleSpace;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface LogicTupleSpace extends ExtendedTupleSpace<LogicTuple, LogicTemplate> {

    default CompletableFuture<LogicTuple> write(String template) {
        return write(new LogicTuple(template));
    }

    default CompletableFuture<LogicTuple> read(String template) {
        return read(new LogicTemplate(template));
    }

    default CompletableFuture<Optional<LogicTuple>> tryRead(String template) {
        return tryRead(new LogicTemplate(template));
    }

    default CompletableFuture<LogicTuple> take(String template) {
        return take(new LogicTemplate(template));
    }

    default CompletableFuture<Optional<LogicTuple>> tryTake(String template) {
        return tryTake(new LogicTemplate(template));
    }

    default CompletableFuture<Collection<? extends LogicTuple>> readAll(String template) {
        return readAll(new LogicTemplate(template));
    }

    default CompletableFuture<Collection<? extends LogicTuple>> takeAll(String template) {
        return takeAll(new LogicTemplate(template));
    }

    default CompletableFuture<Collection<? extends LogicTuple>> writeAll(String template, String... templates) {
        return writeAll(
                Stream.concat(
                        Stream.of(template),
                        Stream.of(templates)
                ).map(LogicTuple::new)
                        .collect(Collectors.toList())
        );
    }

    default CompletableFuture<Integer> getSize() {
        return get().thenApplyAsync(Collection::size);
    }
}
