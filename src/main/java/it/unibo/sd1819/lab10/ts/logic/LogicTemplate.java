package it.unibo.sd1819.lab10.ts.logic;

import alice.tuprolog.*;
import it.unibo.sd1819.lab10.ts.core.Template;
import it.unibo.sd1819.lab10.ts.core.Tuple;

import java.util.Objects;
import java.util.Optional;

public class LogicTemplate implements Template {

    static final String TEMPLATE_WRAPPER = "template";

    private static final Prolog ENGINE = new Prolog();

    private static final Match FAILURE = new MatchImpl(null);
    private final Struct term;

    public LogicTemplate(final String term) {
        this(Term.createTerm(Objects.requireNonNull(term)));
    }

    public LogicTemplate(final Term term) {
        Objects.requireNonNull(term);
        if (term instanceof Struct && ((Struct) term).getName().equals(TEMPLATE_WRAPPER) && ((Struct) term).getArity() == 1) {
            this.term = (Struct) term;
        } else {
            this.term = new Struct(TEMPLATE_WRAPPER, term);
        }
    }

    @Override
    public Match matchWith(Tuple tuple) {
        if (tuple instanceof LogicTuple) {
            final SolveInfo si = ENGINE.solve(new Struct("=", getTemplate(), ((LogicTuple) tuple).getTuple()));
            return new MatchImpl(si);
        }

        return FAILURE;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((term == null) ? 0 : term.toString().hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final LogicTemplate other = (LogicTemplate) obj;
        if (term == null) {
            return other.term == null;
        } else return term.toString().equals(other.term.toString());
    }

    @Override
    public String toString() {
        return term.toString();
    }

    public Struct getTerm() {
        return term;
    }

    public Term getTemplate() {
        return getTerm().getArg(0);
    }

    private static class MatchImpl implements Match {
        private final SolveInfo solveInfo;

        public MatchImpl(SolveInfo solveInfo) {
            this.solveInfo = solveInfo;
        }

        @Override
        public boolean isSuccess() {
            return solveInfo != null && solveInfo.isSuccess();
        }

        @Override
        public <X> Optional<X> get(Object key) {
            if (solveInfo == null) return Optional.empty();

            try {
                return Optional.ofNullable((X) solveInfo.getVarValue(key.toString()));
            } catch (NoSolutionException e) {
                return Optional.empty();
            }
        }
    }
}
