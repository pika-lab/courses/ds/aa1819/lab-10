package it.unibo.sd1819.lab10.ts.core;

public interface ExtendedTupleSpace<T extends Tuple, TT extends Template>
        extends BulkTupleSpace<T, TT>, PredicativeTupleSpace<T, TT> {

}
