package it.unibo.sd1819.lab10.utils;

@FunctionalInterface
public interface Action1<T, E extends Exception> {
    void execute(T arg) throws E;
}