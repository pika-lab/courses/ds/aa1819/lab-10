package it.unibo.sd1819.lab10.utils;

import alice.tuprolog.Term;

/**
 * Objects implementing this interface can be converted into a {@link Term}
 *
 * Classes implementing this interface are expected to expose a public and static factory method named <code>fromTerm</code>,
 * accepting a {@link Term} and returning a new instance of that class produced from the {@link Term}
 * They are also expected to expose a public and static factory method named <code>getTemplate</code> accepting no argument
 * and returning a {@link Term} representing a non-ground template matching ALL possible values possibly produced by {@link #toTerm()}
 * for that class.
 */
public interface PrologSerializable {

    /**
     * Converts the current object into a {@link Term}
     * @return a {@link Term}
     */
    Term toTerm();
}
