package it.unibo.sd1819.lab10.utils;

@FunctionalInterface
public interface Action<E extends Exception> {
    void execute() throws E;
}