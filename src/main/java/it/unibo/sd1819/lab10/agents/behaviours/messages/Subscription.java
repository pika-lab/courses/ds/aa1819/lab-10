package it.unibo.sd1819.lab10.agents.behaviours.messages;

import alice.tuprolog.*;
import it.unibo.sd1819.lab10.agents.AgentId;
import it.unibo.sd1819.lab10.utils.PrologSerializable;
import it.unibo.sd1819.lab10.utils.PrologUtils;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * A data structure aimed at wrapping a subscription for the Publish-Subscribe protocol.
 * It may carry two fields: <ul>
 *     <li>the {@link AgentId} of the <code>subscriber</code></li>
 *     <li>the <code>topic</code> of the subscription, i.e. a (possibly non-ground) term identifying a general topic of interest for the subscriber</li>
 * </ul>
 */
public final class Subscription implements PrologSerializable {

    private static final Prolog PROLOG = new Prolog();
    private static final String PATTERN = "subscription(topic(%s), subscriber(%s))";
    private static final Term TEMPLATE = Term.createTerm(String.format(PATTERN, "T", "S"));

    /**
     * @return a {@link Term} matching all {@link Subscription}s
     */
    public static Term getTemplate() {
        return TEMPLATE;
    }

    /**
     * @param topic an {@link Object} instance whose {@link Term} conversion represent the topic of the {@link Subscription}
     *
     * @return a {@link Term} matching all {@link Subscription}s whose topic matches <code>topic</code>
     */
    public static Term getTemplate(Object topic) {
        return getTemplate(null, topic);
    }

    /**
     * @param topic an {@link Object} instance whose {@link Term} conversion represent the topic of the {@link Subscription}
     * @param subscriber an {@link Object} instance whose {@link Term} conversion represent the subscriber of the {@link Subscription}
     *
     * @return a {@link Term} matching all {@link Subscription}s whose fields match the non-null arguments among the ones above
     */
    public static Term getTemplate(AgentId subscriber, Object topic) {
        final Term goal = PrologUtils.streamToConjunction(
                Stream.concat(
                    Stream.of(
                            subscriber != null ? PrologUtils.unification("S", subscriber.toTerm()) : null,
                            topic != null ? PrologUtils.unification("T", PrologUtils.objectToTerm(topic)) : null
                        ).filter(Objects::nonNull),
                    Stream.of(PrologUtils.unification("X", TEMPLATE))
                )
        );

        final SolveInfo si = PROLOG.solve(goal);
        try {
            return si.getVarValue("X");
        } catch (NoSolutionException e) {
            throw new IllegalStateException(e);
        }
    }

    private final AgentId subscriber;
    private final Object topic;

    public Subscription(AgentId subscriber, Object topic) {
        this.subscriber = Objects.requireNonNull(subscriber);
        this.topic = PrologUtils.objectToTerm(topic);
    }

    public static Subscription fromTerm(Term term) {
        final SolveInfo si = PROLOG.solve(new Struct("=", term, TEMPLATE));
        try {
            final AgentId subscriber = AgentId.fromTerm(si.getVarValue("S"));
            final Term topic = si.getVarValue("T");
            return new Subscription(subscriber, topic);
        } catch (NoSolutionException e) {
            throw new IllegalArgumentException(term.toString());
        }
    }

    public AgentId getSubscriber() {
        return subscriber;
    }

    public Object getTopic() {
        return topic;
    }

    @Override
    public Term toTerm() {
        final Term goal = PrologUtils.streamToConjunction(
                Stream.concat(
                        Stream.of(
                                PrologUtils.unification("S", subscriber.toTerm()),
                                PrologUtils.unification("T", PrologUtils.objectToTerm(topic))
                        ),
                        Stream.of(PrologUtils.unification("X", TEMPLATE))
                )
        );

        final SolveInfo si = PROLOG.solve(goal);
        try {
            return si.getVarValue("X");
        } catch (NoSolutionException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscription that = (Subscription) o;
        return Objects.equals(subscriber, that.subscriber) &&
                Objects.equals(topic, that.topic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subscriber, topic);
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "subscriber=" + subscriber +
                ", topic=" + topic +
                '}';
    }

    public boolean matches(Notification notification) {
        return notification.matches(this);
    }
}
