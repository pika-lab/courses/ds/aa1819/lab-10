package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.AgentController;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * A composite behaviour executing its sub-behaviours in a round-robin fashion
 */
class Parallel implements Behaviour {

    /**
     * Termination criteria for the parallel behaviour
     */
    public enum TerminationCriterion {
        /**
         * The parallel behavior is over as soon as one of its sub-behaviours is over
         */
        ANY,
        /**
         * The parallel behavior is over as soon as all its sub-behaviours are over
         */
        ALL
    }

    private final LinkedList<Behaviour> subBehaviours = new LinkedList<>();
    private final TerminationCriterion terminationCriterion;

    public Parallel(TerminationCriterion terminationCriterion, Collection<Behaviour> bs) {
        this.terminationCriterion = terminationCriterion;
        if (bs.isEmpty()) throw new IllegalArgumentException();
        subBehaviours.addAll(bs);
    }

    public Parallel(TerminationCriterion terminationCriterion, Behaviour b, Behaviour... bs) {
        this.terminationCriterion = terminationCriterion;
        subBehaviours.add(b);
        subBehaviours.addAll(Arrays.asList(bs));
    }

    @Override
    public Behaviour deepClone() {
        return new Parallel(terminationCriterion, subBehaviours.stream().map(Behaviour::deepClone).collect(Collectors.toList()));
    }

    @Override
    public void execute(AgentController ctl) throws Exception {
        // TODO execute the first non-paused sub-behaviour, if any, then move it at the end of the scheduling queue
        throw new IllegalStateException("not implemented");
    }

    @Override
    public boolean isOver() {
        // TODO a parallel is over as soon as its termination criterion is met
        throw new IllegalStateException("not implemented");
    }

    @Override
    public boolean isPaused() {
        // TODO a parallel is paused iff its head is paused
        throw new IllegalStateException("not implemented");
    }

}
