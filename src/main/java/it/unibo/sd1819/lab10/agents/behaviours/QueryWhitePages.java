package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.AgentController;
import it.unibo.sd1819.lab10.agents.AgentId;
import it.unibo.sd1819.lab10.agents.Environment;
import it.unibo.sd1819.lab10.ts.logic.LogicTemplate;
import it.unibo.sd1819.lab10.ts.logic.LogicTuple;
import it.unibo.sd1819.lab10.ts.logic.LogicTupleSpace;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * A behaviour letting agents query the White Pages in order to dynamically discover other agents from within the same environment
 * This assumes other agents have already register their {@link AgentId}s as tuples within the White Pages tuple space,
 * that is, the tuple spaces returned by {@link Environment#getWhitePagesTupleSpace()}, whose name is {@link Environment#getWhitePagesTupleSpaceName()}
 *
 * Before ending, the behaviour will invoke the {@link #onQueryResult(AgentController, List)} callback, providing a list of
 * {@link AgentId}s containing the IDs of all agents currently registered in the White Pages
 */
abstract class QueryWhitePages extends LindaOperation<Collection<? extends LogicTuple>> {


    @Override
    public final CompletableFuture<Collection<? extends LogicTuple>> invokeOnTupleSpace(AgentController ctl, LogicTupleSpace tupleSpace) {
        throw new IllegalStateException("not implemented");
    }

    @Override
    public final String getTupleSpaceName(AgentController ctl) {
        throw new IllegalStateException("not implemented");
    }

    @Override
    public final void onResult(AgentController ctl, Collection<? extends LogicTuple> result) throws Exception {
        throw new IllegalStateException("not implemented");
    }

    public abstract void onQueryResult(AgentController ctl, List<AgentId> agentIds) throws Exception;

}
