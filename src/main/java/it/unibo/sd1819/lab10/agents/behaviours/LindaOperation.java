package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.Agent;
import it.unibo.sd1819.lab10.agents.AgentController;
import it.unibo.sd1819.lab10.agents.AgentId;
import it.unibo.sd1819.lab10.agents.Environment;
import it.unibo.sd1819.lab10.ts.logic.LogicTupleSpace;

import java.util.concurrent.CompletableFuture;

abstract class LindaOperation<T> extends Await<T> {

    @Override
    public final CompletableFuture<T> getPromise(AgentController ctl) {
        return invokeOnTupleSpace(ctl, ctl.getTupleSpace(getTupleSpaceName(ctl)));
    }

    /**
     * Invoke the desired operation on the tuple space selected by {@link #getTupleSpaceName(AgentController)}
     * @param ctl an {@link AgentController} object, useful if you need to build up Linda operation arguments as a function
     *            of the {@link Environment} name or the {@link Agent}'s
     *            {@link AgentId}
     * @param tupleSpace is a {@link LogicTupleSpace} whose name is {@link #getTupleSpaceName(AgentController)}.
     *                   It must be a valid tuple space existing within the {@link Environment}
     *                   returned by <code>ctl.getEnvironment()</code>
     * @return the {@link CompletableFuture} produced by the operation invoked on <code>tupleSpace</code>
     */
    public abstract CompletableFuture<T> invokeOnTupleSpace(AgentController ctl, LogicTupleSpace tupleSpace);

    /**
     * Retrieve a name for the tuple space a Linda operation should be invoked on
     * @param ctl an {@link AgentController} object, useful if you need to build up the tuple space name as a function
     *            of the {@link Environment} name or the {@link Agent}'s
     *            {@link AgentId}
     * @return a non-null {@link String}
     */
    public abstract String getTupleSpaceName(AgentController ctl);

}
