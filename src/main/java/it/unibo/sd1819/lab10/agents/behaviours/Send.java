package it.unibo.sd1819.lab10.agents.behaviours;

import alice.tuprolog.Term;
import it.unibo.sd1819.lab10.agents.AgentController;
import it.unibo.sd1819.lab10.agents.AgentId;
import it.unibo.sd1819.lab10.agents.behaviours.messages.Message;
import it.unibo.sd1819.lab10.ts.logic.LogicTuple;
import it.unibo.sd1819.lab10.ts.logic.LogicTupleSpace;

import java.util.concurrent.CompletableFuture;

abstract class Send extends LindaOperation<LogicTuple> {

    @Override
    public final CompletableFuture<LogicTuple> invokeOnTupleSpace(AgentController ctl, LogicTupleSpace tupleSpace) {
        final AgentId receiver = ctl.generateAgentId(getReceiver());
        final Message message = new Message(ctl.getAgentId(), receiver, getPayload());
        ctl.log("Sending `%s` to agent \"%s\"", message.getPayload(), message.getReceiver());
        return tupleSpace.write(new LogicTuple(message.toTerm()));
    }

    @Override
    public final String getTupleSpaceName(AgentController ctl) {
        return "inbox-" + ctl.generateAgentId(getReceiver());
    }

    @Override
    public final void onResult(AgentController ctl, LogicTuple result) throws Exception {
        final Message parsedMessage = Message.fromTerm(result.getTuple());
        onMessageSent(ctl, parsedMessage);
    }

    public abstract String getReceiver();

    public abstract Term getPayload();

    public abstract void onMessageSent(AgentController ctl, Message message) throws Exception;
}
