package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.Agent;
import it.unibo.sd1819.lab10.agents.AgentController;
import it.unibo.sd1819.lab10.utils.Action;
import it.unibo.sd1819.lab10.utils.Action1;

import java.util.function.Supplier;

@FunctionalInterface
public interface Behaviour {

    /**
     * When the current behaviour is added to some agent, its {@link #execute(AgentController)} method is executed
     * over and over again until the {@link #isOver()} method returns true
     *
     * @param ctl an {@link AgentController} making it possible to control the agent from within the behaviour,
     *            other than accessing useful information about the agent and the environment it is running into
     *
     * @throws Exception exceptions occurring from within the behaviour are propagated to the
     * {@link it.unibo.sd1819.lab10.agents.BaseAgent#onUncaughtError(Exception)} method
     */
    void execute(AgentController ctl) throws Exception;

    /**
     * Test whether the behaviour is paused or not. In case it is, its {@link #execute(AgentController)} method won't be executed,
     * but it won't be removed from the agent's scheduling queue. This implies that its {@link #execute(AgentController)}
     * method will be executed again if this method returns <code>false</code>.
     *
     * By default, behaviours are never paused
     *
     * @return a <code>boolean</code> value which is <code>true</code> iff the behaviour is paused
     */
    default boolean isPaused() {
        return false;
    }

    /**
     * Test whether the behaviour has finished or not. In case it is, its {@link #execute(AgentController)} method won't
     * be executed anymore, since it will be removed from the agent's scheduling queue.
     * This method is always tested after {@link #execute(AgentController)} has been executed.
     * This implies that, for each behaviour, the {@link #execute(AgentController)} method should be executed at least once.
     *
     * By default, behaviours are always over, i.e., they are one-shot behaviours
     *
     * @return a <code>boolean</code> value which is <code>true</code> iff the behaviour is over
     */
    default boolean isOver() {
        return true;
    }

    /**
     * This method returns a reset copy of the current behaviour, ready to be executed again.
     * In case this behaviour is composed by some sub-behaviours, the {@link #deepClone()} method should recursively
     * clone the whole sub-behaviours hierarchy.
     *
     * In case the current behaviour is totally stateless, the {@link #deepClone()} method may return a reference to
     * <code>this</code> (which, in fact, is the default implementation).
     *
     * In case the current behaviour is stateful (or it is composed by stateful sub-behaviours) this method must return
     * a fresh new copy of the behaviour
     *
     * @return a {@link Behaviour} behaving exactly as the current one
     */
    default Behaviour deepClone() {
        return this;
    }

    // ^^^ the 4 methods above are the important ones

    default Behaviour addToAgent(Agent agent) {
        // TODO use Agent.addBehaviour, then return this
        throw new IllegalStateException("not implemented");
    }

    default Behaviour removeFromAgent(Agent agent) {
        // TODO use Agent.removeBehaviour then return this
        throw new IllegalStateException("not implemented");
    }

    default Behaviour andThen(Behaviour b, Behaviour... bs) {
        return new Sequence(this, b, bs);
    }

    default Behaviour andThen(Action<? extends Exception> action) {
        return andThen(Behaviours.of(action));
    }

    default Behaviour andThen(Action1<AgentController, ? extends Exception> action) {
        return andThen(Behaviours.of(action));
    }

    default Behaviour repeatWhile(Supplier<Boolean> condition) {
        return new DoWhile(Behaviour.this) {

            @Override
            public boolean condition() {
                return condition.get();
            }

        };
    }

    default Behaviour repeatUntil(Supplier<Boolean> condition) {
        return repeatWhile(() -> !condition.get());
    }

    default Behaviour repeatForEver() {
        return repeatWhile(() -> true);
    }

}