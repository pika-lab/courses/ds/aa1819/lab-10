package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.AgentController;

import java.time.Duration;
import java.time.OffsetDateTime;

class Wait implements Behaviour {

    private final Duration duration;
    private boolean started;
    private OffsetDateTime clock;

    Wait(Duration duration) {
        this.duration = duration;
    }

    @Override
    public Behaviour deepClone() {
        return new Wait(duration);
    }

    @Override
    public void execute(AgentController ctl) throws Exception {
        if (started) {
            // TODO if now - clock is greater than duration this behaviour may end
            // Consider using the ChronoUnit.MILLIS.between() method
            throw new IllegalStateException("not implemented");
        } else {
            started = true;
            clock = OffsetDateTime.now();
        }
    }

    @Override
    public boolean isOver() {
        throw new IllegalStateException("not implemented");
    }
}
