package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.AgentController;

import java.util.concurrent.CompletableFuture;

/**
 * A behaviour waiting for a {@link CompletableFuture} (or "promise") to be completed
 */
abstract class Await<T> implements Behaviour {

    private enum Phase {
        CREATED, PAUSED, COMPLETED, DONE;
    }

    private Phase phase = Phase.CREATED;
    private CompletableFuture<T> promiseCache;

    @Override
    public void execute(AgentController ctl) throws Exception {
        if (phase == Phase.CREATED) {
            promiseCache = getPromise(ctl);

            // TODO switch to phase COMPLETED as soon as promise is completed
            // TODO make sure the promise's thenRun is executed on the agent's executor (ctl may be useful)

            phase = Phase.PAUSED;
        } else if (phase == Phase.COMPLETED) {
            // TODO handle the promise result with onResult

            phase = Phase.DONE;
        } else {
            throw new IllegalStateException("This should never happen");
        }
    }

    /**
     * Handles the completion of the promise
     * @param ctl
     * @param result the value which completed the future
     * @throws Exception
     */
    public abstract void onResult(AgentController ctl, T result) throws Exception;

    /**
     * Generates a promise to be eventually completed
     * @param ctl
     * @return
     */
    public abstract CompletableFuture<T> getPromise(AgentController ctl);

    /**
     * The current behaviour is paused until its promise has been completed
     */
    @Override
    public boolean isPaused() {
        return phase == Phase.PAUSED;
    }

    /**
     * The current behaviour ends after the promise has been completed and the corresponding value handled by {@link #onResult(AgentController, Object)}
     */
    @Override
    public boolean isOver() {
        return phase == Phase.DONE;
    }

    /**
     * Concrete sub classes are strictly required to override this method
     */
    @Override
    public Behaviour deepClone() {
        // do not edit this method!
        // This is ok, do not remove this exception, just override this method in subclasses :)
        throw new IllegalStateException("You must override the deepClone method in class " + this.getClass().getName());
    }
}
