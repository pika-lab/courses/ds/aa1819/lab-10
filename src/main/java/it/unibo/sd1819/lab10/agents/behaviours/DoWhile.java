package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.AgentController;

import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.Collectors;

class DoWhile extends Sequence { // TODO notice that DoWhile extends Sequence

    /**
     * A backup deep copy of initial behaviours composing the do-while loop
     */
    private final Deque<Behaviour> behavioursBackup;
    private boolean isEndOfRound = false;

    public DoWhile(Behaviour b, Behaviour... bs) {
        super(b, bs);
        behavioursBackup = getSubBehaviours().stream().map(Behaviour::deepClone).collect(Collectors.toCollection(LinkedList::new));
    }

    public DoWhile(Collection<Behaviour> bs) {
        super(bs);
        behavioursBackup = getSubBehaviours().stream().map(Behaviour::deepClone).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Behaviour deepClone() {
        return new DoWhile(getSubBehaviours().stream().map(Behaviour::deepClone).collect(Collectors.toList()));
    }

    @Override
    public void execute(AgentController ctl) throws Exception {
        // TODO ensure that the condition is only tested at the end of each round

        super.execute(ctl); // TODO this may be useful
        throw new IllegalStateException("not implemented");
    }

    @Override
    public boolean isOver() {
        return isEndOfRound && !condition();
    }

    /**
     * Test the loop permanence condition, i.e., whether another round of the loop should be executed or not.
     * This condition is only tested after each round of the loop, making this behaviour analogous to the following pseudo-code:
     * </br>
     * <code>
     *     do {
     *         getSubBehaviours().get(0).execute()
     *         ...
     *         getSubBehaviours().get(N).execute()
     *     } while(condition())
     * </code>
     *
     * @return
     */
    public boolean condition() {
        return true; // By default, loops last forever (this can be overridden)
    }

}
