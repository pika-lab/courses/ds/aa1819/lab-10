package it.unibo.sd1819.lab10.agents.behaviours;

import alice.tuprolog.Term;
import it.unibo.sd1819.lab10.agents.AgentController;
import it.unibo.sd1819.lab10.agents.behaviours.messages.Notification;
import it.unibo.sd1819.lab10.agents.behaviours.messages.Subscription;
import it.unibo.sd1819.lab10.ts.logic.LogicTemplate;
import it.unibo.sd1819.lab10.ts.logic.LogicTuple;

import java.util.List;
import java.util.stream.Collectors;

abstract class Publish implements Behaviour {

    private List<Subscription> subscriptions;

    private final Behaviour getSubscriptionBehaviour = null; // TODO implement me

    private final Behaviour pushNotificationsBehaviour = null; // TODO implement me

    @Override
    public void execute(AgentController ctl) throws Exception {
        if (subscriptions == null) {
            getSubscriptionBehaviour.execute(ctl);
        } else {
            pushNotificationsBehaviour.execute(ctl);
        }
    }

    @Override
    public boolean isPaused() {
        if (subscriptions == null) {
            return getSubscriptionBehaviour.isPaused();
        } else {
            return pushNotificationsBehaviour.isPaused();
        }
    }

    @Override
    public boolean isOver() {
        if (subscriptions == null) {
            return getSubscriptionBehaviour.isOver();
        } else {
            return pushNotificationsBehaviour.isOver();
        }
    }

    @Override
    public Behaviour deepClone() {
        // do not edit this method!
        throw new IllegalStateException("You must override the deepClone method in class " + this.getClass().getName());
    }

    private Object getTopic(AgentController ctl) {
        return getNotification(ctl).getTopic();
    }

    private Term getSubscriptionTemplate(AgentController ctl) {
        return Subscription.getTemplate(getTopic(ctl));
    }

    public abstract Notification getNotification(AgentController ctl);
}
