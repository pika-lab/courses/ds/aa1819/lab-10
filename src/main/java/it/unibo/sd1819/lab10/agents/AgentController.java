package it.unibo.sd1819.lab10.agents;

import it.unibo.sd1819.lab10.agents.behaviours.Behaviour;
import it.unibo.sd1819.lab10.ts.logic.LogicTupleSpace;

import java.util.concurrent.ExecutorService;

/**
 * A façade for an agent, exposing utility methods for governing an agent
 */
public final class AgentController {

    private final BaseAgent agent;

    private AgentController(BaseAgent agent) {
        this.agent = agent;
    }

    static AgentController of(BaseAgent agent) {
        return new AgentController(agent);
    }

    private boolean isBehaviouralAgent() {
        return agent instanceof Agent;
    }

    private Agent asAgent() {
        return (Agent) agent;
    }

    public void stop() {
        agent.stop();
    }

    public void restart() {
        agent.restart();
    }

    public void pause() {
        agent.pause();
    }

    public void resume() {
        agent.resume();
    }

    public void resumeIfPaused() {
        agent.resumeIfPaused();
    }

    public void log(Object format, Object... args) {
        agent.log(format, args);
    }

    public ExecutorService getEngine() {
        return agent.getEngine();
    }

    public AgentId getAgentId() {
        return agent.getAgentId();
    }

    public AgentId generateAgentId(String raw) {
        return getEnvironment().generateAgentId(raw);
    }

    public void addBehaviour(Behaviour b, Behaviour... bs) {
        asAgent().addBehaviour(b);
        asAgent().addBehaviour(bs);
    }

    public void removeBehaviour(Behaviour b, Behaviour... bs) {
        asAgent().removeBehaviour(b);
        asAgent().removeBehaviour(bs);
    }

    public Environment getEnvironment() {
        return agent.getEnvironment();
    }

    public LogicTupleSpace getTupleSpace(String name) {
        return agent.getEnvironment().getTupleSpace(name);
    }
}
