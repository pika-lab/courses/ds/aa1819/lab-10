package it.unibo.sd1819.lab10.agents;

import it.unibo.sd1819.lab10.agents.behaviours.Behaviour;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

public class Agent extends BaseAgent {

    private final Queue<Behaviour> toDoList = new LinkedList<>();

    public Agent(String name) {
        super(name);
    }

    public Agent(AgentId id) {
        super(id);
    }

    @Override
    public void onBegin() throws Exception {
        // TODO register the agent's on the white pages tuple space and then call setup
        // TODO setup must be called before the first onRun
        // notice that you can do this later
    }

    public void setup() {
        // Does nothing by default
    }

    @Override
    public final void onRun() throws Exception {
        // TODO implement a round-robin scheduler for the toDoList
        // TODO paused behaviours are simply skipped
        // TODO isPaused-test is performed before executing a behaviour
        // TODO behaviours which are over are simply dropped
        // TODO isOver-test is performed after the behaviour had been executed
        // TODO if no non-paused & non-over behaviour currently exists, the agent should be paused
        // TODO remember to resume the agent as soon as some behaviour is added
        throw new IllegalStateException("not implemented");
    }

    @Override
    public void onEnd() throws Exception {
        // TODO cleanup the agent's id from the white pages
        // TODO then call tearDown
    }

    public void tearDown() {
        // Does nothing by default
    }

    public void addBehaviour(Collection<Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.addAll(behaviours);
            // TODO do something here
        }
    }

    public void addBehaviour(Behaviour... behaviours) {
        addBehaviour(Arrays.asList(behaviours));
    }

    public void removeBehaviour(Collection<Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.removeAll(behaviours);
        }
    }

    public void removeBehaviour(Behaviour... behaviours) {
        removeBehaviour(Arrays.asList(behaviours));
    }
}
