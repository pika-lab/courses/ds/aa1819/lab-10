package it.unibo.sd1819.lab10.agents.behaviours.messages;

import alice.tuprolog.*;
import it.unibo.sd1819.lab10.agents.AgentId;
import it.unibo.sd1819.lab10.utils.PrologSerializable;
import it.unibo.sd1819.lab10.utils.PrologUtils;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * A data structure aimed at wrapping a notification for the Publish-Subscribe protocol.
 * It may carry two fields: <ul>
 *     <li>the <code>topic</code> of the notification, i.e. a (possibly ground) term identifying the main topic of the notification</li>
 *     <li>a <code>payload</code>, i.e. a term representing the payload of the notification</li>
 * </ul>
 */
public final class Notification implements PrologSerializable {

    private static final Prolog PROLOG = new Prolog();
    private static final String PATTERN = "notification(topic(%s), content(%s))";
    private static final Term TEMPLATE = Term.createTerm(String.format(PATTERN, "T", "C"));


    /**
     * @return a {@link Term} matching all {@link Notification}s
     */
    public static Term getTemplate() {
        return TEMPLATE;
    }

    /**
     * @param topic an {@link Object} instance whose {@link Term} conversion represent the topic of the {@link Notification}
     *
     * @return a {@link Term} matching all {@link Notification}s whose topic matches <code>topic</code>
     */
    public static Term getTemplate(Object topic) {
        return getTemplate(topic, null);
    }

    /**
     * @param topic an {@link Object} instance whose {@link Term} conversion represent the topic of the {@link Notification}
     * @param content an {@link Object} instance whose {@link Term} conversion represent the payload of the {@link Notification}
     *
     * @return a {@link Term} matching all {@link Notification}s whose fields match the non-null arguments among the ones above
     */
    public static Term getTemplate(Object topic, Object content) {
        final Term goal = PrologUtils.streamToConjunction(
                Stream.concat(
                    Stream.of(
                            topic != null ? PrologUtils.unification("T", PrologUtils.objectToTerm(topic)) : null,
                            topic != null ? PrologUtils.unification("C", PrologUtils.objectToTerm(content)) : null
                        ).filter(Objects::nonNull),
                    Stream.of(PrologUtils.unification("X", TEMPLATE))
                )
        );

        final SolveInfo si = PROLOG.solve(goal);
        try {
            return si.getVarValue("X");
        } catch (NoSolutionException e) {
            throw new IllegalStateException(e);
        }
    }

    private final Object topic;
    private final Object content;

    public Notification(Object topic, Object content) {
        this.topic = PrologUtils.objectToTerm(topic);
        this.content = PrologUtils.objectToTerm(content);
    }

    public static Notification fromTerm(Term term) {
        final SolveInfo si = PROLOG.solve(new Struct("=", term, TEMPLATE));
        try {
            final Term topic = si.getVarValue("T");
            final Term content = si.getVarValue("C");
            return new Notification(topic, content);
        } catch (NoSolutionException e) {
            throw new IllegalArgumentException(term.toString());
        }
    }

    public Object getTopic() {
        return topic;
    }

    public Object getContent() {
        return content;
    }

    @Override
    public Term toTerm() {
        final Term goal = PrologUtils.streamToConjunction(
                Stream.concat(
                        Stream.of(
                                PrologUtils.unification("T", PrologUtils.objectToTerm(topic)),
                                PrologUtils.unification("C", PrologUtils.objectToTerm(content))
                        ),
                        Stream.of(PrologUtils.unification("X", TEMPLATE))
                )
        );

        final SolveInfo si = PROLOG.solve(goal);
        try {
            return si.getVarValue("X");
        } catch (NoSolutionException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notification that = (Notification) o;
        return Objects.equals(topic, that.topic) &&
                Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic, content);
    }

    @Override
    public String toString() {
        return "Notification{" +
                "topic=" + topic +
                ", content=" + content +
                '}';
    }

    public boolean matches(Subscription subscription) {
        return PrologUtils.objectToTerm(topic).match(PrologUtils.objectToTerm(subscription.getTopic()));
    }
}
