package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.AgentController;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A composite behaviour executing its sub-behaviours sequentially
 */
class Sequence implements Behaviour {

    private final Deque<Behaviour> subBehaviours = new LinkedList<>();

    public Sequence(Collection<Behaviour> bs) {
        if (bs.isEmpty()) throw new IllegalArgumentException();
        subBehaviours.addAll(bs);
    }

    public Sequence(Behaviour b, Behaviour... bs) {
        subBehaviours.add(b);
        subBehaviours.addAll(Arrays.asList(bs));
    }

    public Sequence(Behaviour b1, Behaviour b2, Behaviour... bs) {
        subBehaviours.add(b1);
        subBehaviours.add(b2);
        subBehaviours.addAll(Arrays.asList(bs));
    }

    @Override
    public Behaviour deepClone() {
        return new Sequence(subBehaviours.stream().map(Behaviour::deepClone).collect(Collectors.toList()));
    }

    @Override
    public boolean isPaused() {
        // TODO a sequence is paused iff its head is paused
        throw new IllegalStateException("not implemented");
    }

    @Override
    public void execute(AgentController ctl) throws Exception {
        // TODO execute the first sub-behaviour until it's other then forget about it
        throw new IllegalStateException("not implemented");
    }

    protected List<Behaviour> getSubBehaviours() {
        return new ArrayList<>(subBehaviours);
    }

    protected int countResidualSubBehaviours() {
        return subBehaviours.size();
    }

    protected void addSubBehaviours(Collection<Behaviour> subBehaviours) {
        this.subBehaviours.addAll(subBehaviours);
    }

    @Override
    public boolean isOver() {
        // TODO a sequence is over iff it has executed all its sub-behaviours
        throw new IllegalStateException("not implemented");
    }

}
