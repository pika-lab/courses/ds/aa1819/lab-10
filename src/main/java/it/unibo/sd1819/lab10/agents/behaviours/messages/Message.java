package it.unibo.sd1819.lab10.agents.behaviours.messages;

import alice.tuprolog.*;
import it.unibo.sd1819.lab10.agents.AgentId;
import it.unibo.sd1819.lab10.utils.PrologSerializable;
import it.unibo.sd1819.lab10.utils.PrologUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * A data structure aimed at wrapping a message to be exchanged among two agents.
 * It may carry four fields: <ul>
 *     <li>the message <code>sender</code></li>
 *     <li>the message <code>receiver</code> a.k.a. the recipient of the message</li>
 *     <li>a <code>conversation_id</code>, i.e., a UUID which should be equal among all messages exchanges within the scope of a protocol</li>
 *     <li>a <code>payload</code>, i.e. a term</li>
 * </ul>
 */
public final class Message implements PrologSerializable {

    private static final Prolog PROLOG = new Prolog();
    private static final String PATTERN = "message(sender(%s), recipient(%s), conversation_id(%s), payload(%s))";
    private static final Term TEMPLATE = Term.createTerm(String.format(PATTERN, "S", "R", "Cid", "P"));


    /**
     * @return a {@link Term} matching all {@link Message}s
     */
    public static Term getTemplate() {
        return TEMPLATE;
    }

    /**
     * @param receiver a non-null {@link AgentId} instance representing the {@link Message} receiver
     *
     * @return a {@link Term} matching all {@link Message}s whose receiver is equal to <code>receiver</code>
     */
    public static Term getTemplate(AgentId receiver) {
        return getTemplate(receiver, null);
    }

    /**
     * @param receiver a non-null {@link AgentId} instance representing the {@link Message} receiver
     * @param uuid a non-null {@link UUID} instance representing the {@link Message} conversation ID
     *
     * @return a {@link Term} matching all {@link Message}s whose receiver is equal to <code>receiver</code> and whose
     * conversation ID is equal to <code>uuid</code>
     */
    public static Term getTemplate(AgentId receiver, UUID uuid) {
        return getTemplate(null, receiver, uuid, null);
    }

    /**
     *
     * @param receiver an {@link AgentId} instance representing the {@link Message} receiver
     * @param uuid an {@link UUID} instance representing the {@link Message} conversation ID
     * @param sender an {@link AgentId} instance representing the {@link Message} sender
     * @param payload an {@link Object} instance whose {@link Term} conversion represent the payload of the {@link Message}
     *
     * @returna {@link Term} matching all {@link Message}s whose fields are equal to the non-null arguments among the ones above
     */
    public static Term getTemplate(AgentId sender, AgentId receiver, UUID uuid, Object payload) {
        Term goal = PrologUtils.streamToConjunction(
                Stream.concat(
                    Stream.of(
                            sender != null ? PrologUtils.unification("S", sender.toTerm()) : null,
                            receiver != null ? PrologUtils.unification("R", receiver.toTerm()) : null,
                            uuid != null ? PrologUtils.unification("Cid", new Struct(uuid.toString())) : null,
                            payload != null ? PrologUtils.unification("P", PrologUtils.objectToTerm(payload)) : null
                        ).filter(Objects::nonNull),
                    Stream.of(PrologUtils.unification("M", TEMPLATE))
                )
        );

        final SolveInfo si = PROLOG.solve(goal);
        try {
            return si.getVarValue("M");
        } catch (NoSolutionException e) {
            throw new IllegalStateException(e);
        }
    }

    private final AgentId sender;
    private final AgentId receiver;
    private UUID conversationId;
    private final Term payload;

    public Message(AgentId sender, AgentId payload) {
        this(sender, null, null, payload);
    }

    public Message(AgentId sender, AgentId receiver, Object payload) {
        this(sender, receiver, null, payload);
    }

    public Message(AgentId sender, AgentId receiver, UUID uuid, Object payload) {
        this.sender = Objects.requireNonNull(sender);
        this.receiver = receiver;
        this.conversationId = uuid;
        this.payload = PrologUtils.objectToTerm(payload);
    }

    public static Message fromTerm(Term term) {
        final SolveInfo si = PROLOG.solve(new Struct("=", term, TEMPLATE));
        try {
            final AgentId sender = AgentId.fromTerm(si.getVarValue("S"));
            final AgentId receiver = AgentId.fromTerm(si.getVarValue("R"));
            final Term idTerm = si.getVarValue("Cid");
            final UUID id = idTerm instanceof Var ? null : UUID.fromString(((Struct)idTerm).getName());
            final Term payload = si.getVarValue("P");
            return new Message(sender, receiver, id, payload);
        } catch (NoSolutionException e) {
            throw new IllegalArgumentException(term.toString());
        }
    }

    public Term getPayload() {
        return payload;
    }

    public String getPayloadAsString() {
        return payload.toString();
    }

    public AgentId getSender() {
        return sender;
    }

    public AgentId getReceiver() {
        return receiver;
    }

    public UUID getConversationId() {
        return conversationId;
    }

    public Message setGeneratedConversationId() {
        conversationId = UUID.randomUUID();
        return this;
    }

    public Message buildReply(Term payload) {
        return new Message(
            receiver,
            sender,
            conversationId,
            payload
        );
    }

    @Override
    public Struct toTerm() {
        return new Struct("message",
                new Struct("sender", sender.toTerm()),
                new Struct("recipient", Optional.ofNullable(receiver)
                        .map(AgentId::toTerm)
                        .map(it -> (Term) it)
                        .orElseGet(Var::new)),
                new Struct("conversation_id",
                        conversationId == null ? new Var() : new Struct(conversationId.toString())
                ),
                new Struct("payload", payload)
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(sender, message.sender) &&
                Objects.equals(receiver, message.receiver) &&
                Objects.equals(conversationId, message.conversationId) &&
                Objects.equals(payload, message.payload);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, receiver, payload.toString());
    }

    @Override
    public String toString() {
        return "Message{" +
                "sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", conversationId='" + conversationId + '\'' +
                ", payload=" + payload +
                '}';
    }
}
