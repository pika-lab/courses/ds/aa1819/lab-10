package it.unibo.sd1819.lab10.agents.behaviours;

import it.unibo.sd1819.lab10.agents.AgentController;
import it.unibo.sd1819.lab10.agents.behaviours.messages.Message;
import it.unibo.sd1819.lab10.ts.logic.LogicTuple;
import it.unibo.sd1819.lab10.ts.logic.LogicTupleSpace;

import java.util.concurrent.CompletableFuture;

/**
 * A behaviour performing a suspensive receive for any message directed toward the agent executing the current behaviour.
 * While waiting for the message, the current behaviour is paused but the agent may keep executing other bahaviours
 *
 * The {@link Receive} behaviour is assumed to invoke its {@link #onMessageReceived(AgentController, Message)} callback
 * as soon as a message is received by the agent executing the behaviour.
 * After that the behaviour is over.
 */
abstract class Receive extends LindaOperation<LogicTuple> {

    @Override
    public final CompletableFuture<LogicTuple> invokeOnTupleSpace(AgentController ctl, LogicTupleSpace tupleSpace) {
        // TODO perform a `take` operation of a message directed toward the current agent, on tupleSpace
        throw new IllegalStateException("not implemented");
    }

    @Override
    public final String getTupleSpaceName(AgentController ctl) {
        // TODO return the name of the tuple space used as inbox for the agent executing the current behaviour
        // the inbox tuple space name should be `inbox-<ReceivingAgentName>` as you can infer looking at the Send class
        throw new IllegalStateException("not implemented");
    }

    @Override
    public final void onResult(AgentController ctl, LogicTuple result) throws Exception {
        // TODO parse the LogicTuple's content as a Message an then invoke the onMessageReceived callback
        throw new IllegalStateException("not implemented");
    }

    public abstract void onMessageReceived(AgentController ctl, Message message) throws Exception;
}
