package it.unibo.sd1819.lab10.agents.behaviours;

import alice.tuprolog.Term;
import it.unibo.sd1819.lab10.agents.AgentController;
import it.unibo.sd1819.lab10.agents.AgentId;
import it.unibo.sd1819.lab10.agents.behaviours.messages.Message;
import it.unibo.sd1819.lab10.agents.behaviours.messages.Notification;
import it.unibo.sd1819.lab10.agents.behaviours.messages.Subscription;
import it.unibo.sd1819.lab10.ts.logic.LogicTupleSpace;
import it.unibo.sd1819.lab10.utils.Action;
import it.unibo.sd1819.lab10.utils.Action1;
import it.unibo.sd1819.lab10.utils.Action2;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Behaviours {
    private Behaviours() {
    }

    public static Behaviour of(Action<? extends Exception> action) {
        return ctl -> action.execute();
    }

    public static Behaviour of(Action1<AgentController, ? extends Exception> action) {
        return action::execute;
    }

    public static Behaviour sequence(Behaviour b, Behaviour... bs) {
        return new Sequence(b, bs);
    }

    public static Behaviour allOf(Behaviour b, Behaviour... bs) {
        return new Parallel(Parallel.TerminationCriterion.ALL, b, bs);
    }

    public static Behaviour anyOf(Behaviour b, Behaviour... bs) {
        return new Parallel(Parallel.TerminationCriterion.ANY, b, bs);
    }

    public static Behaviour wait(Duration duration) {
        return new Wait(duration);
    }

    public static <T> Behaviour await(CompletableFuture<T> promise, Action1<T, ? extends Exception> action) {
        return await(promise, (ctl, x) -> action.execute(x));
    }

    public static <T> Behaviour await(CompletableFuture<T> promise, Action2<AgentController, T, ? extends Exception> action) {
        return new Await<T>() {

            @Override
            public CompletableFuture<T> getPromise(AgentController ctl) {
                return promise;
            }

            @Override
            public void onResult(AgentController ctl, T result) throws Exception {
                action.execute(ctl, result);
            }

            @Override
            public Behaviour deepClone() {
                return await(promise, action);
            }
        };
    }

    public static <T> Behaviour linda(String tupleSpace, Function<LogicTupleSpace, CompletableFuture<T>> invoker, Action1<T, ? extends Exception> action) {
        return linda(ctl -> tupleSpace, (ctl, ts) -> invoker.apply(ts), (ctl, x) -> action.execute(x));
    }

    public static <T> Behaviour linda(Function<AgentController, String> nameGetter, Function<LogicTupleSpace, CompletableFuture<T>> invoker, Action1<T, ? extends Exception> action) {
        return linda(nameGetter, (ctl, ts) -> invoker.apply(ts), (ctl, x) -> action.execute(x));
    }

    public static <T> Behaviour linda(String name, Function<LogicTupleSpace, CompletableFuture<T>> invoker, Action2<AgentController, T, ? extends Exception> action) {
        return linda(ctl -> name, (ctl, ts) -> invoker.apply(ts), action);
    }

    public static <T> Behaviour linda(String tupleSpace, BiFunction<AgentController, LogicTupleSpace, CompletableFuture<T>> invoker, Action1<T, ? extends Exception> action) {
        return linda(ctl -> tupleSpace, invoker, (ctl, x) -> action.execute(x));
    }

    public static <T> Behaviour linda(Function<AgentController, String> nameGetter, BiFunction<AgentController, LogicTupleSpace, CompletableFuture<T>> invoker, Action1<T, ? extends Exception> action) {
        return linda(nameGetter, invoker, (ctl, x) -> action.execute(x));
    }

    public static <T> Behaviour linda(String name, BiFunction<AgentController, LogicTupleSpace, CompletableFuture<T>> invoker, Action2<AgentController, T, ? extends Exception> action) {
        return linda(ctl -> name, invoker, action);
    }

    public static <T> Behaviour linda(Function<AgentController, String> nameGetter, BiFunction<AgentController, LogicTupleSpace, CompletableFuture<T>> invoker, Action2<AgentController, T, ? extends Exception> action) {
        return new LindaOperation<T>() {
            @Override
            public CompletableFuture<T> invokeOnTupleSpace(AgentController ctl, LogicTupleSpace tupleSpace) {
                return invoker.apply(ctl, tupleSpace);
            }

            @Override
            public String getTupleSpaceName(AgentController ctl) {
                return nameGetter.apply(ctl);
            }

            @Override
            public void onResult(AgentController ctl, T result) throws Exception {
                action.execute(ctl, result);
            }

            @Override
            public Behaviour deepClone() {
                return linda(nameGetter, invoker, action);
            }
        };
    }

    public static Behaviour send(AgentId recipient, String term) {
        return send(recipient.toString(), Term.createTerm(term), (ctl, m) -> {
        });
    }

    public static Behaviour send(String recipient, String term) {
        return send(recipient, Term.createTerm(term), (ctl, m) -> {
        });
    }

    public static Behaviour send(AgentId recipient, Term payload) {
        return send(recipient.toString(), payload, (ctl, m) -> {
        });
    }

    public static Behaviour send(String recipient, Term payload) {
        return send(recipient, payload, (ctl, m) -> {
        });
    }

    public static Behaviour send(AgentId recipient, String payload, Action2<AgentController, Message, ?> onMessageSent) {
        return send(recipient.toString(), Term.createTerm(payload), onMessageSent);
    }

    public static Behaviour send(String recipient, String payload, Action2<AgentController, Message, ?> onMessageSent) {
        return send(recipient, Term.createTerm(payload), onMessageSent);
    }

    public static Behaviour send(AgentId recipient, Term payload, Action2<AgentController, Message, ?> onMessageSent) {
        return send(recipient.toString(), payload, onMessageSent);
    }

    public static Behaviour send(String recipient, Term payload, Action2<AgentController, Message, ?> onMessageSent) {
        return new Send() {
            @Override
            public String getReceiver() {
                return recipient;
            }

            @Override
            public Term getPayload() {
                return payload;
            }

            @Override
            public void onMessageSent(AgentController ctl, Message message) throws Exception {
                onMessageSent.execute(ctl, message);
            }

            @Override
            public Behaviour deepClone() {
                return send(recipient, payload, onMessageSent);
            }
        };
    }

    public static Behaviour receive(Action1<Message, ?> onMessageReceived) {
        return receive((ctl, x) -> onMessageReceived.execute(x));
    }

    public static Behaviour receive(Action2<AgentController, Message, ?> onMessageReceived) {
        return new Receive() {
            @Override
            public void onMessageReceived(AgentController ctl, Message message) throws Exception {
                onMessageReceived.execute(ctl, message);
            }

            @Override
            public Behaviour deepClone() {
                return receive(onMessageReceived);
            }
        };
    }

    public static Behaviour queryWhitePages(Action1<List<AgentId>, ?> onAgentIdsReceived) {
        return queryWhitePages((ctl, x) -> onAgentIdsReceived.execute(x));
    }

    public static Behaviour queryWhitePages(Action2<AgentController, List<AgentId>, ?> onAgentIdsReceived) {
        return new QueryWhitePages() {
            @Override
            public void onQueryResult(AgentController ctl, List<AgentId> agentIds) throws Exception {
                onAgentIdsReceived.execute(ctl, agentIds);
            }

            @Override
            public Behaviour deepClone() {
                return queryWhitePages(onAgentIdsReceived);
            }
        };
    }

    public static Behaviour publish(Object topic, Object content) {
        return publish(ctl -> new Notification(topic, content));
    }

    public static Behaviour publish(Notification notification) {
        return publish(ctl -> notification);
    }

    public static Behaviour publish(Function<AgentController, Notification> notificationGetter) {
        return new Publish() {

            @Override
            public Notification getNotification(AgentController ctl) {
                return notificationGetter.apply(ctl);
            }

            @Override
            public Behaviour deepClone() {
                return publish(notificationGetter);
            }
        };
    }

    public static Behaviour subscribe(Object topicGetter, Action1<Notification, ? extends Exception> onNotified) {
        return subscribe(topicGetter, (ctl, n) -> onNotified.execute(n));
    }

    public static Behaviour subscribe(Object topicGetter, Action2<AgentController,Notification, ? extends Exception> onNotified) {
        return subscribe(ctl -> topicGetter, (ctl, s) -> {}, onNotified);
    }

    public static Behaviour subscribe(Object topicGetter, Action1<Subscription, ? extends Exception> onSubscribed, Action1<Notification, ? extends Exception> onNotified) {
        return subscribe(topicGetter, onSubscribed, (ctl, n) -> onNotified.execute(n));
    }

    public static Behaviour subscribe(Object topicGetter, Action1<Subscription, ? extends Exception> onSubscribed, Action2<AgentController,Notification, ? extends Exception> onNotified) {
        return subscribe(ctl -> topicGetter, (ctl, s) -> onSubscribed.execute(s), onNotified);
    }

    public static Behaviour subscribe(Object topicGetter, Action2<AgentController, Subscription, ? extends Exception> onSubscribed, Action1<Notification, ? extends Exception> onNotified) {
        return subscribe(topicGetter, onSubscribed, (ctl, n) -> onNotified.execute(n));
    }

    public static Behaviour subscribe(Object topicGetter, Action2<AgentController, Subscription, ? extends Exception> onSubscribed, Action2<AgentController,Notification, ? extends Exception> onNotified) {
        return subscribe(ctl -> topicGetter, onSubscribed, onNotified);
    }


    public static Behaviour subscribe(Function<AgentController, Object> topicGetter, Action2<AgentController, Subscription, ? extends Exception> onSubscribed, Action2<AgentController,Notification, ? extends Exception> onNotified) {
        return new Subscribe() {
            @Override
            public Object getTopic(AgentController ctl) {
                return topicGetter.apply(ctl);
            }

            @Override
            public void onSubscribed(AgentController ctl, Subscription subscription) throws Exception {
                onSubscribed.execute(ctl, subscription);
            }

            @Override
            public void onNotified(AgentController ctl, Notification subscription) throws Exception {
                onNotified.execute(ctl, subscription);
            }
        };
    }
}
