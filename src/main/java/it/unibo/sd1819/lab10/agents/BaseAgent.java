package it.unibo.sd1819.lab10.agents;

import java.time.Duration;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.*;

public abstract class BaseAgent {

    private final CompletableFuture<Void> termination = new CompletableFuture<>();
    private AgentId id;
    private States state = States.CREATED;
    private AndThen nextOperation = null;
    private Environment environment;

    public BaseAgent(String name) {
        name = Optional.ofNullable(name).orElseGet(() -> getClass().getSimpleName() + "_" + System.identityHashCode(BaseAgent.this));
        id = new AgentId(name);
    }
    public BaseAgent(AgentId id) {
        this.id = Objects.requireNonNull(id);
    }

    private void ensureCurrentStateIs(States state) {
        ensureCurrentStateIs(EnumSet.of(state));
    }

    private void ensureCurrentStateIs(EnumSet<States> states) {
        if (!currentStateIsOneOf(states)) {
            throw new IllegalStateException("Illegal state: " + this.state + ", expected: " + states);
        }
    }

    private boolean currentStateIs(States state) {
        return Objects.equals(this.state, state);
    }

    private boolean currentStateIsOneOf(EnumSet<States> states) {
        return states.contains(this.state);
    }

    private void doStateTransition(AndThen whatToDo) {
        switch (state) {
            case CREATED:
                doStateTransitionFromCreated(whatToDo);
                break;
            case STARTED:
                doStateTransitionFromStarted(whatToDo);
                break;
            case RUNNING:
                doStateTransitionFromRunning(whatToDo);
                break;
            case PAUSED:
                doStateTransitionFromPaused(whatToDo);
                break;
            case STOPPED:
                doStateTransitionFromStopped(whatToDo);
                break;
            default:
                throw new IllegalStateException("Illegal state: " + state);

        }
    }

    protected void doStateTransitionFromCreated(AndThen whatToDo) {
        switch (whatToDo) {
            case CONTINUE:
                state = States.STARTED;
                doBegin();
                break;
            default:
                throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromStarted(AndThen whatToDo) {
        doStateTransitionFromRunning(whatToDo);
    }

    protected void doStateTransitionFromRunning(AndThen whatToDo) {
        switch (whatToDo) {
            case PAUSE:
                state = States.PAUSED;
                break;
            case RESTART:
                state = States.STARTED;
                doBegin();
                break;
            case STOP:
                state = States.STOPPED;
                doEnd();
                break;
            case CONTINUE:
                state = States.RUNNING;
                doRun();
                break;
            default:
                throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromPaused(AndThen whatToDo) {
        doStateTransitionFromRunning(whatToDo);
    }

    protected void doStateTransitionFromStopped(AndThen whatToDo) {
        switch (whatToDo) {
            case RESTART:
                state = States.STARTED;
                doBegin();
                break;
            case STOP:
            case CONTINUE:
                state = States.STOPPED;
                termination.complete(null);
                break;
            default:
                throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    private void doBegin() {
        if (getEnvironment() == null) {
            throw new IllegalStateException();
        }
        ensureCurrentStateIs(States.STARTED);
        getEngine().submit(this::begin);
    }

    private void begin() {
        nextOperation = AndThen.CONTINUE;
        try {
            onBegin();
        } catch (Exception e) {
            nextOperation = onError(e);
        } finally {
            doStateTransition(nextOperation);
        }
    }

    public void onBegin() throws Exception {
        // does nothing by default
    }

    private void doRun() {
        ensureCurrentStateIs(EnumSet.of(States.PAUSED, States.RUNNING));
        getEngine().submit(this::run);
    }

    private void run() {
        nextOperation = AndThen.CONTINUE;
        try {
            onRun();
        } catch (Exception e) {
            nextOperation = onError(e);
        } finally {
            doStateTransition(nextOperation);
        }
    }

    public abstract void onRun() throws Exception;

    private void doEnd() {
        getEngine().submit(this::end);
    }

    private void end() {
        nextOperation = AndThen.CONTINUE;
        try {
            onEnd();
        } catch (Exception e) {
            nextOperation = onError(e);
        } finally {
            doStateTransition(nextOperation);
        }
    }

    public void onEnd() throws Exception {
        // does nothing by default
    }

    private AndThen onError(Exception e) {
        return onUncaughtError(e);
    }

    public AndThen onUncaughtError(Exception e) {
        e.printStackTrace();
        return AndThen.STOP;
    }

    public final void start() {
        ensureCurrentStateIs(States.CREATED);
        doStateTransition(AndThen.CONTINUE);
    }

    protected final void resume() {
        ensureCurrentStateIs(States.PAUSED);
        doStateTransition(AndThen.CONTINUE);
    }

    // TODO notice me
    protected final void resumeIfPaused() {
        if (currentStateIs(States.PAUSED)) {
            doStateTransition(AndThen.CONTINUE);
        }
    }

    protected final void pause() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        nextOperation = AndThen.PAUSE;
    }

    protected final void stop() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        if (currentStateIs(States.PAUSED)) {
            doStateTransition(AndThen.STOP);
        } else {
            nextOperation = AndThen.STOP;
        }
    }

    protected final void restart() {
        ensureCurrentStateIs(EnumSet.complementOf(EnumSet.of(States.CREATED)));
        nextOperation = AndThen.RESTART;
    }

    protected Environment getEnvironment() {
        return environment;
    }

    protected final void setEnvironment(Environment environment) {
        this.environment = Objects.requireNonNull(environment);
        id = new AgentId(id.getLocalName(), environment.getName());
    }

    protected ExecutorService getEngine() {
        return getEnvironment() != null ? getEnvironment().getEngine() : null;
    }

    protected final void log(Object format, Object... args) {
        System.out.printf("[" + getAgentId() + "] " + format + "\n", args);
    }

    public AgentId getAgentId() {
        return id;
    }

    public void await(Duration duration) throws InterruptedException, ExecutionException, TimeoutException {
        termination.get(duration.toMillis(), TimeUnit.MILLISECONDS);
    }

    public void await() throws InterruptedException, ExecutionException, TimeoutException {
        termination.get(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }

    protected enum AndThen {
        CONTINUE, RESTART, PAUSE, STOP
    }

    protected enum States {
        CREATED, STARTED, RUNNING, PAUSED, STOPPED
    }

}
