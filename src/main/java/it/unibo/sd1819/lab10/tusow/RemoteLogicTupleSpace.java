package it.unibo.sd1819.lab10.tusow;

import alice.tuprolog.Struct;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import it.unibo.sd1819.lab10.ts.logic.LogicTemplate;
import it.unibo.sd1819.lab10.ts.logic.LogicTuple;
import it.unibo.sd1819.lab10.ts.logic.LogicTupleSpace;
import it.unibo.sd1819.lab10.utils.PrologUtils;
import org.javatuples.Pair;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RemoteLogicTupleSpace implements LogicTupleSpace {

    private final String host;
    private final int port;
    private final String root;
    private final String name;

    private final HttpClient client;

    public RemoteLogicTupleSpace(String host, int port, String name) {
        this(host, port, "data_spaces", name);
    }

    public RemoteLogicTupleSpace(String host, int port, String root, String name) {
        this.host = Objects.requireNonNull(host);
        this.port = port;
        this.root = Objects.requireNonNull(root);
        this.name = Objects.requireNonNull(name);

        this.client = Vertx.vertx().createHttpClient();
    }

    private static Pair<String, Object> query(String key, Object value) {
        return Pair.with(key, value);
    }

    private static String urlEncode(Object x) {
        try {
            return URLEncoder.encode(x.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(x.toString());
        }
    }

    @Override
    public CompletableFuture<Collection<? extends LogicTuple>> readAll(LogicTemplate template) {
        return request(HttpMethod.GET, query("template", template.getTemplate()), query("bulk", true))
                .thenApplyAsync(body -> Optional.ofNullable(body).orElse("[]"))
                .thenApplyAsync(PrologUtils::parseList)
                .thenApplyAsync(lts -> lts.map(LogicTuple::new))
                .thenApplyAsync(lst -> lst.collect(Collectors.toList()));
    }

    @Override
    public CompletableFuture<Collection<? extends LogicTuple>> takeAll(LogicTemplate template) {
        return request(HttpMethod.DELETE, query("template", template.getTemplate()), query("bulk", true))
                .thenApplyAsync(body -> Optional.ofNullable(body).orElse("[]"))
                .thenApplyAsync(PrologUtils::parseList)
                .thenApplyAsync(lts -> lts.map(LogicTuple::new))
                .thenApplyAsync(lst -> lst.collect(Collectors.toList()));
    }

    @Override
    public CompletableFuture<Collection<? extends LogicTuple>> writeAll(Collection<? extends LogicTuple> tuples) {
        final Struct listOfTuples = PrologUtils.streamToList(tuples.stream().map(LogicTuple::getTuple));
        return request(HttpMethod.POST, listOfTuples, query("sync", true), query("bulk", true))
                .thenApply(result -> tuples);
    }

    @Override
    public CompletableFuture<Optional<LogicTuple>> tryTake(LogicTemplate template) {
        return request(HttpMethod.DELETE, query("template", template.getTemplate()), query("predicative", true))
                .thenApplyAsync(body -> Optional.ofNullable(body).orElse("[]"))
                .thenApplyAsync(PrologUtils::parseList)
                .thenApplyAsync(lts -> lts.map(LogicTuple::new))
                .thenApplyAsync(Stream::findFirst);
    }

    @Override
    public CompletableFuture<Optional<LogicTuple>> tryRead(LogicTemplate template) {
        return request(HttpMethod.GET, query("template", template.getTemplate()), query("predicative", true))
                .thenApplyAsync(body -> Optional.ofNullable(body).orElse("[]"))
                .thenApplyAsync(PrologUtils::parseList)
                .thenApplyAsync(lts -> lts.map(LogicTuple::new))
                .thenApplyAsync(Stream::findFirst);
    }

    @Override
    public CompletableFuture<LogicTuple> read(LogicTemplate template) {
        return request(HttpMethod.GET, query("template", template.getTemplate()))
                .thenApplyAsync(body -> Optional.ofNullable(body).orElse("[]"))
                .thenApplyAsync(PrologUtils::parseList).thenApplyAsync(Stream::findFirst)
                .thenApplyAsync(Optional::get)
                .thenApplyAsync(LogicTuple::new);
    }

    @Override
    public CompletableFuture<LogicTuple> take(LogicTemplate template) {
        return request(HttpMethod.DELETE, query("template", template.getTemplate()))
                .thenApplyAsync(body -> Optional.ofNullable(body).orElse("[]"))
                .thenApplyAsync(PrologUtils::parseList)
                .thenApplyAsync(Stream::findFirst)
                .thenApplyAsync(Optional::get)
                .thenApplyAsync(LogicTuple::new);
    }

    @Override
    public CompletableFuture<LogicTuple> write(LogicTuple tuple) {
        return request(HttpMethod.POST, tuple.getTuple(), query("sync", true))
                .thenApply(result -> tuple);
    }

    @Override
    public CompletableFuture<Collection<? extends LogicTuple>> get() {
        return request(HttpMethod.GET)
                .thenApplyAsync(body -> Optional.ofNullable(body).orElse("[]"))
                .thenApplyAsync(PrologUtils::parseList)
                .thenApplyAsync(lts -> lts.map(LogicTuple::new).collect(Collectors.toList()));
    }

    @Override
    public String getName() {
        return name;
    }

    private String getPath() {
        return String.format("/%s/prolog/%s", root, name);
    }

    private CompletableFuture<String> request(HttpMethod method, Pair<String, Object>... queries) {
        return request(method, null, queries);
    }

    private CompletableFuture<String> request(HttpMethod method, Object body, Pair<String, Object>... queries) {
        final String queryString = Arrays.stream(queries)
                .map(kv -> kv.getValue0() + "=" + urlEncode(kv.getValue1().toString()))
                .collect(Collectors.joining("&", "?", ""));

        final CompletableFuture<String> result = new CompletableFuture<>();

        final HttpClientRequest request = client.request(method, port, host, getPath() + queryString)
                .putHeader("Content-Type", "application/prolog")
                .putHeader("Accept", "application/prolog")
                .handler(response -> {
                    if (response.statusCode() == 200) {
                        response.bodyHandler(resBody -> {
                            result.complete(resBody.toString());
                        });
                    } else if (response.statusCode() == 201 || response.statusCode() == 204) {
                        result.complete(null);
                    } else {
                        result.completeExceptionally(new UnexpectedResponseException(response.statusCode(), response.statusMessage()));
                    }
                });

        if (body == null) {
            request.end();
        } else {
            request.end(body.toString());
        }

        return result;
    }


}
