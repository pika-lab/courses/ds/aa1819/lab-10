package it.unibo.sd1819.lab10.tusow;

public class UnexpectedResponseException extends Exception {
    private final int statusCode;

    public UnexpectedResponseException(int statusCode) {
        super("Unexpected status code from server: " + statusCode);
        this.statusCode = statusCode;
    }

    public UnexpectedResponseException(int statusCode, String message) {
        super("Unexpected status code from server: " + statusCode + "\nMessage: " + message);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
