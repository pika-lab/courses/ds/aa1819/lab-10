package it.unibo.sd1819.lab10.tusow;

import it.unibo.tusow.web.Main$;

public class TuSoWService {
    public static void main(String[] args) {
        start(args);
    }

    public static void start(String... args) {
        Main$.MODULE$.main(args);
    }

    public static void stop() {
        Main$.MODULE$.terminate();
    }
}
